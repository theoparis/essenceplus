package me.creepinson.mod.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import me.creepinson.creepinoutils.base.BaseItem;
import me.creepinson.mod.util.ModUtils;

public class ItemStickOfLightning extends BaseItem {
	public ItemStickOfLightning(String name, CreativeTabs tab) {
		super(new ResourceLocation(ModUtils.MOD_ID, name), tab);
		this.setMaxDamage(50);

	}

	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
		if (!world.isRemote) {
			RayTraceResult pos = player.rayTrace(100, 20);
			double x = pos.getBlockPos().getX();
			double y = pos.getBlockPos().getY();
			double z = pos.getBlockPos().getZ();

			world.addWeatherEffect(new EntityLightningBolt(world, x, y, z, bFull3D));
			player.getHeldItem(hand).damageItem(1, player);
			return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, new ItemStack(this));
		} else {
			return new ActionResult<ItemStack>(EnumActionResult.PASS, new ItemStack(this));
		}

	}

}
