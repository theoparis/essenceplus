package me.creepinson.mod.item;

import java.util.List;

import me.creepinson.mod.core.EnumHandler;
import me.creepinson.mod.core.EnumHandler.Essences;
import me.creepinson.mod.util.ModUtils;
import me.creepinson.mod.core.ItemHandler;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import me.creepinson.creepinoutils.base.BaseItem;

public class ItemEssence extends BaseItem {

	@Override
	public String getTranslationKey(ItemStack stack) {
		for (int i = 0; i < Essences.values().length; i++) {
			if (stack.getItemDamage() == i) {
				return EnumHandler.Essences.values()[i].getName() + "essence";
			}

			else {
				continue;
			}

		}
		return EnumHandler.Essences.fire.getName() + "essence";

	}

	public ItemEssence(String name, CreativeTabs tab) {
		super(new ResourceLocation(ModUtils.MOD_ID, name), tab);
		this.setHasSubtypes(true);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		for (int i = 0; i < Essences.values().length; i++) {
			ItemStack stack = new ItemStack(this, 1, i);
			if (getCreativeTab() != tab)
				return;

			items.add(stack);
		}
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);

		if (ItemStack.areItemsEqual(stack, new ItemStack(ItemHandler.essence, 1, 3))) {

			tooltip.add(TextFormatting.LIGHT_PURPLE + "Magical...");

		}

	}

}