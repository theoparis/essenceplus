package me.creepinson.mod.world.dimension;

import net.minecraft.world.DimensionType;
import net.minecraftforge.common.DimensionManager;

public class EPDimensionRegistry {

	public static void mainRegistry(){
		
		registerDimensions();
		
	}

	public static final int creepolaDimensionID = DimensionManager.getNextFreeDimId();
	public static final DimensionType CREEPOLA_NORMAL = DimensionType.register("creepola_dimension", "", creepolaDimensionID, DimensionTypeCreepola.class, false);
	public static void registerDimensions() {
	
		
		DimensionManager.registerDimension(creepolaDimensionID, CREEPOLA_NORMAL);
		
	}
	
	
	
	
}
