package me.creepinson.mod.world.biome;

import me.creepinson.mod.core.ConfigHandler;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.BiomeProperties;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class EPBiomeRegistry {

	
	
	
	public static void mainRegistry(){
		
		EPBiomeRegistry.initBiomes();
		EPBiomeRegistry.registerBiomes();
		
	}
	
	public static Biome creepop;
	
	public static int creepopBiomeID;
	
	
	
	public static void initBiomes(){
		creepopBiomeID = ConfigHandler.creepopBiomeID;
		 
		creepop = new BiomeCreepop(new BiomeProperties("Creepop").setRainfall(0.0f).setRainDisabled().setWaterColor(52326).setBaseHeight(0.1f).setHeightVariation(2.0f));
		creepop.setRegistryName("creepop");
		
	}
	
	public static void registerBiomes(){

		ForgeRegistries.BIOMES.register(creepop);
		
	
	}
	
}
