package me.creepinson.mod.util;

import java.util.Random;

public class ModUtils {

	public static final String MOD_ID = "@MODID@";
	public static final String MOD_NAME = "@MODNAME@";
	public static final String MOD_VERSION = "@MODVERSION@";
	public static final String MOD_URL = "https://gitlab.com/creepinson/essenceplus";
	public static final Random random = new Random();

}
