package me.creepinson.mod.util.world;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.ChunkPrimer;
import net.minecraft.world.gen.MapGenBase;

public class MapGenBlocks extends MapGenBase {

	private int w,h,d;
	private IBlockState block;


	public MapGenBlocks(int width, int height, int depth, IBlockState block) {
		this.w = width;
		this.h = height;
		this.d = depth;
		this.block = block;
	}

	
	@Override
	protected void recursiveGenerate(World worldIn, int chunkX, int chunkZ, int originalX, int originalZ,
			ChunkPrimer chunkPrimerIn) {
		super.recursiveGenerate(worldIn, chunkX, chunkZ, originalX, originalZ, chunkPrimerIn);
			int minHeight = 70;
			int chance = 25;
			for(int i = 0; i < chance; i++) {
				int x = chunkX * 16 + rand.nextInt(16);
				int y = minHeight + rand.nextInt(16);
				int z = chunkZ * 16 + rand.nextInt(16);
				(new WorldGenBlocks(w,h,d,block)).generate(world, rand, new BlockPos(x, y, z));
			}
	}

}