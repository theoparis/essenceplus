package me.creepinson.mod.tileentity;

import me.creepinson.mod.core.ItemHandler;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;

public class TileAnalyzer extends TileEntity implements IInventory {

	private NonNullList<ItemStack> stacks = NonNullList.<ItemStack>withSize(1, ItemStack.EMPTY);

	public int getSizeInventory() {
		return this.stacks.size();
	}

	public boolean isEmpty() {
		for (ItemStack itemstack : this.stacks) {
			if (!itemstack.isEmpty()) {
				return false;
			}
		}

		return true;
	}

	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		this.stacks = NonNullList.<ItemStack>withSize(this.getSizeInventory(), ItemStack.EMPTY);

	}

	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);

		if (!this.stacks.isEmpty()) {
			ItemStackHelper.saveAllItems(compound, this.stacks);
		}

		return compound;
	}

	public int getInventoryStackLimit() {
		return 64;
	}

	public NonNullList<ItemStack> getItems() {
		return this.stacks;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public boolean hasCustomName() {
		return false;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return this.stacks.get(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		this.stacks.get(index).setCount(count);
		return this.stacks.get(index);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		this.stacks.remove(index);
		return null;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		this.stacks.set(index, stack);
	}

	@Override
	public boolean isUsableByPlayer(EntityPlayer player) {
		return true;
	}

	@Override
	public void openInventory(EntityPlayer player) {

	}

	@Override
	public void closeInventory(EntityPlayer player) {

	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return this.stacks.contains(stack) && this.stacks.get(index).equals(stack);
	}

	@Override
	public int getField(int id) {
		return 0;
	}

	@Override
	public void setField(int id, int value) {

	}

	@Override
	public int getFieldCount() {
		return 0;
	}

	@Override
	public void clear() {
		this.stacks.clear();
	}

	public boolean analyze() {

		if (this.stacks.get(0).getItem() == ItemHandler.syringe && this.stacks.get(0).getMetadata() != 0) {
			this.stacks.get(0).getTagCompound().setBoolean("analyzed", true);
			EntityItem e = new EntityItem(world, pos.getX(), pos.getY() + 1, pos.getZ(), this.stacks.get(0));
			world.spawnEntity(e);
			this.setInventorySlotContents(0, ItemStack.EMPTY);
			return true;
		}

		return false;
	}

}
