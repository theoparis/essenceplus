package me.creepinson.mod.core;

import me.creepinson.creepinoutils.base.BaseBlock;
import me.creepinson.mod.block.BlockAnalyzer;
import me.creepinson.mod.block.BlockCreepolaGrass;
import me.creepinson.mod.block.BlockCreepolaPortal;
import me.creepinson.mod.block.BlockCreepySnow;
import me.creepinson.mod.tileentity.TileAnalyzer;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

public class BlockHandler {

    public static BaseBlock.Ore oreCopper = new BaseBlock.Ore(new ResourceLocation(ModUtils.MOD_ID, "ore_copper"),
            CreativeTabs.BUILDING_BLOCKS, 3.5F, 1.8F);
    public static BlockAnalyzer analyzer = new BlockAnalyzer(EssencePlus.instance.creativeTab, 3.5f, 8f, 2, "pickaxe");
    public static BaseBlock.Ore ore_steel = new BaseBlock.Ore(new ResourceLocation(ModUtils.MOD_ID, "ore_steel"),
            EssencePlus.instance.creativeTab, 6.5F, 3.0F, 2, "pickaxe");
    public static BlockCreepolaPortal creepola_portal = new BlockCreepolaPortal();
    public static BlockCreepolaGrass creepola_grass = new BlockCreepolaGrass();
    public static BaseBlock creepola_dirt = new BaseBlock(Material.GRASS,
            new ResourceLocation(ModUtils.MOD_ID, "creepola_dirt"), EssencePlus.instance.creativeTab)
            .setSound(SoundType.GROUND);
    public static BlockCreepySnow creepySnow = new BlockCreepySnow("creepy_snow").setHardness(0.2F);

    public static void register(IForgeRegistry<Block> registry) {
        registry.registerAll(oreCopper, analyzer, creepola_portal, creepola_dirt, creepola_grass, ore_steel,
                creepySnow);
    }

    public static void registerItemBlocks(IForgeRegistry<Item> registry) {
        registry.registerAll(oreCopper.createItemBlock(), analyzer.createItemBlock(),
                new ItemBlock(creepola_portal).setRegistryName(creepola_portal.getRegistryName()),
                creepola_dirt.createItemBlock(), creepola_grass.createItemBlock(), creepySnow.createItemBlock(),
                ore_steel.createItemBlock());
        GameRegistry.registerTileEntity(TileAnalyzer.class, analyzer.getRegistryName());

    }

    public static void registerModels() {
        EssencePlus.registerItemRenderer(oreCopper.createItemBlock(), 0, oreCopper.getRegistryName().getPath());
        EssencePlus.registerItemRenderer(analyzer.createItemBlock(), 0, analyzer.getRegistryName().getPath());
        EssencePlus.registerItemRenderer(creepola_dirt.createItemBlock(), 0, creepola_dirt.getRegistryName().getPath());
        EssencePlus.registerItemRenderer(creepola_grass.createItemBlock(), 0, creepola_grass.getRegistryName().getPath());
        EssencePlus.registerItemRenderer(creepySnow.createItemBlock(), 0, creepySnow.getRegistryName().getPath());
        EssencePlus.registerItemRenderer(ore_steel.createItemBlock(), 0, ore_steel.getRegistryName().getPath());
    }

}