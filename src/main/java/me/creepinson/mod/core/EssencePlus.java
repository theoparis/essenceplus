package me.creepinson.mod.core;

import me.creepinson.creepinoutils.base.BaseMod;
import me.creepinson.creepinoutils.base.CreativeTab;
import me.creepinson.creepinoutils.util.CreativeTabCallback;
import me.creepinson.mod.recipe.LifeCoreRecipe;
import me.creepinson.mod.util.ModUtils;
import me.creepinson.mod.world.biome.EPBiomeRegistry;
import me.creepinson.mod.world.dimension.EPDimensionRegistry;
import me.creepinson.mod.world.generation.WorldHandlerVillageDistrict;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.File;

@Mod(modid = ModUtils.MOD_ID, name = ModUtils.MOD_NAME, version = ModUtils.MOD_VERSION)
@Mod.EventBusSubscriber
public class EssencePlus extends BaseMod {

    @Mod.Instance(ModUtils.MOD_ID)
    public static EssencePlus instance;

    private static File configDir;

    public EssencePlus() {
        super(ModUtils.MOD_URL, null, ModUtils.MOD_ID, ModUtils.MOD_VERSION);

    }

    public static File getConfigDir() {
        return configDir;
    }

    // TODO: move sounds to soundhandler
    public static SoundEvent CRAEOL_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "craeol.death"));
    public static SoundEvent CRAEOL_LIVING = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "craeol.ambient"));
    public static SoundEvent CRAEOL_HURT = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "craeol.hurt"));
    public static SoundEvent ALEXION_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "alexion.death"));
    public static SoundEvent ALEXION_LIVING = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "alexion.ambient"));
    public static SoundEvent ALEXION_HURT = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "alexion.hurt"));
    public static SoundEvent NEOTE_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "neote.death"));
    public static SoundEvent NEOTE_LIVING = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "neote.ambient"));
    public static SoundEvent NEOTE_HURT = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "neote.hurt"));
    public static SoundEvent CREEPINO_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "creepinodeath"));
    public static SoundEvent CREEPINO_HURT = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "creepinohurt"));
    public static SoundEvent CREEPINO_SCREECH = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "creepinoscreech"));
    public static SoundEvent CREEPERINO_LIVING = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "creeprliving"));
    public static SoundEvent CREEPERINO_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MOD_ID, "creeprdeath"));

    @EventHandler
    public void serverStarting(final FMLServerStartingEvent event) {

    }

    public static void registerItemRenderer(Item item, int meta, String id) {
        ModelLoader.setCustomModelResourceLocation(item, meta,
                new ModelResourceLocation(new ResourceLocation(ModUtils.MOD_ID, id), "inventory"));
    }


    @Mod.EventHandler
    public void preInit(final FMLPreInitializationEvent event) {
        super.preInit(event, new CreativeTabCallback() {
            public void init(CreativeTab tab) {
                tab.setItem(new ItemStack(ItemHandler.essence, 1, 0));
            }
        });

        WorldHandlerVillageDistrict.preInit();

        if (event.getSide() == Side.CLIENT)
            clientPreInit(event);

        EntityHandler.registerEntities();

        LootTableList.register(new ResourceLocation(ModUtils.MOD_ID, "creepino_loot"));
        LootTableList.register(new ResourceLocation(ModUtils.MOD_ID, "alexion_loot"));

    }

    private void registerRenderers() {
        EntityHandler.registerRenderers();
    }

    public void clientPreInit(FMLPreInitializationEvent event) {
        registerRenderers();
    }

    public void clientInit(FMLInitializationEvent event) {
        ModelBakery.registerItemVariants(ItemHandler.essence, new ResourceLocation(ModUtils.MOD_ID, "fireessence"),
                new ResourceLocation(ModUtils.MOD_ID, "bloodessence"),
                new ResourceLocation(ModUtils.MOD_ID, "lifeessence"), new ResourceLocation(ModUtils.MOD_ID, "magicessence"), new ResourceLocation(ModUtils.MOD_ID, "grassessence"));
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public static void registerSounds(RegistryEvent.Register<SoundEvent> event) {


        event.getRegistry().registerAll(EssencePlus.CREEPINO_DEATH.setRegistryName(EssencePlus.CREEPINO_DEATH.getSoundName()),
                EssencePlus.CREEPINO_HURT.setRegistryName(EssencePlus.CREEPINO_HURT.getSoundName()), EssencePlus.CREEPINO_SCREECH.setRegistryName(EssencePlus.CREEPINO_SCREECH.getSoundName()),
                EssencePlus.NEOTE_DEATH.setRegistryName(EssencePlus.NEOTE_DEATH.getSoundName()), EssencePlus.NEOTE_LIVING.setRegistryName(EssencePlus.NEOTE_LIVING.getSoundName()),
                EssencePlus.NEOTE_HURT.setRegistryName(EssencePlus.NEOTE_HURT.getSoundName()), EssencePlus.ALEXION_DEATH.setRegistryName(EssencePlus.ALEXION_DEATH.getSoundName()),
                EssencePlus.ALEXION_LIVING.setRegistryName(EssencePlus.ALEXION_LIVING.getSoundName()), EssencePlus.ALEXION_HURT.setRegistryName(EssencePlus.ALEXION_HURT.getSoundName()),
                EssencePlus.CRAEOL_DEATH.setRegistryName(EssencePlus.CRAEOL_DEATH.getSoundName()), EssencePlus.CRAEOL_LIVING.setRegistryName(EssencePlus.CRAEOL_LIVING.getSoundName()),
                EssencePlus.CRAEOL_HURT.setRegistryName(EssencePlus.CRAEOL_HURT.getSoundName()), EssencePlus.CREEPERINO_LIVING.setRegistryName(EssencePlus.CREEPERINO_LIVING.getSoundName()),
                EssencePlus.CREEPERINO_DEATH.setRegistryName(EssencePlus.CREEPERINO_DEATH.getSoundName()));
    }

    @Mod.EventHandler
    @Override
    public void init(final FMLInitializationEvent event) {
        super.init(event);
        ItemHandler.oreDictionary();

        EPDimensionRegistry.mainRegistry();
        EPBiomeRegistry.mainRegistry();
        if (event.getSide() == Side.CLIENT)
            clientInit(event);

        EntityHandler.addSpawns();
        WorldHandlerVillageDistrict.registerComponent(WorldHandlerVillageDistrict.Wall.class, 20, 1, 1);
    }

    @Mod.EventHandler
    @Override
    public void postInit(final FMLPostInitializationEvent event) {
        super.postInit(event);
    }

    @SubscribeEvent
    public static void registerBlocks(final RegistryEvent.Register<Block> event) {
        BlockHandler.register(event.getRegistry());
    }

    @SubscribeEvent
    public static void registerItems(final RegistryEvent.Register<Item> event) {
        ItemHandler.init();
        ItemHandler.register(event.getRegistry());
        BlockHandler.registerItemBlocks(event.getRegistry());
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public static void registerModels(final ModelRegistryEvent event) {
        BlockHandler.registerModels();
        ItemHandler.registerModels();
    }

    @SubscribeEvent
    public static void registerRecipes(RegistryEvent.Register<IRecipe> event) {
        event.getRegistry().registerAll(new LifeCoreRecipe(false), new LifeCoreRecipe(true));

    }

}
