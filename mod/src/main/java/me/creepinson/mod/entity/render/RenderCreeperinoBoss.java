package me.creepinson.mod.entity.render;

import me.creepinson.mod.entity.boss.EntityCreeperinoBoss;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderCreeperinoBoss extends RenderLiving<EntityCreeperinoBoss> {

	public RenderCreeperinoBoss(RenderManager rendermanagerIn, ModelBase modelbaseIn, float shadowsizeIn) {
		super(rendermanagerIn, modelbaseIn, shadowsizeIn);

	}

	private final ResourceLocation creepinoRed = new ResourceLocation(ModUtils.MODID,
			"textures/entity/creepino/creepino_red.png");

	protected ResourceLocation getEntityTexture(EntityCreeperinoBoss entity) {
		return creepinoRed;

	}
	
	@Override
	public void doRender(EntityCreeperinoBoss entity, double x, double y, double z, float entityYaw,
			float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.popMatrix();
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
	}

}
