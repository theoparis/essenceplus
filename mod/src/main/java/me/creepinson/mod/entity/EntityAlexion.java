package me.creepinson.mod.entity;

import java.util.Random;

import me.creepinson.mod.core.MainMod;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIEatGrass;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class EntityAlexion extends EntityCreature {

	public EntityAlexion(World world) {
		super(world);
		experienceValue = 10;
		this.isImmuneToFire = false;
		this.tasks.addTask(0, new EntityAISwimming(this));
		this.tasks.addTask(6, new EntityAIWander(this, 1.0D));
		this.tasks.addTask(8, new EntityAILookIdle(this));
		this.targetTasks.addTask(7, new EntityAIHurtByTarget(this, false));
		this.tasks.addTask(3, new EntityAIWander(this, 0.8D));
		this.tasks.addTask(1, new EntityAISwimming(this));
		this.tasks.addTask(3, new EntityAIEatGrass(this));

	}

	protected void applyEntityAttributes() {
		super.applyEntityAttributes();
		this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25D);
		this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);
		if (this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE) != null)
			this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(2.0D);
	}

	@Override
	public void onLivingUpdate() {

		super.onLivingUpdate();
		World par1World = this.world;
		Random ran = this.rand;
		if (true)
			for (int l = 0; l < 9; ++l) {
				par1World.spawnParticle(EnumParticleTypes.PORTAL, posX, posY, posZ, 0.0D, 0.0D, 0.0D);
			}
	}

	@Override
	protected ResourceLocation getLootTable() {
		return new ResourceLocation(ModUtils.MODID, "alexion_loot");

	}	
	
	@Override
	protected net.minecraft.util.SoundEvent getAmbientSound() {
		return MainMod.ALEXION_LIVING;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
		return MainMod.ALEXION_HURT;
	}

	@Override
	protected net.minecraft.util.SoundEvent getDeathSound() {
		return MainMod.ALEXION_DEATH;
	}

	@Override
	public void onStruckByLightning(EntityLightningBolt entityLightningBolt) {
		super.onStruckByLightning(entityLightningBolt);
		int i = (int) this.posX;
		int j = (int) this.posY;
		int k = (int) this.posZ;
		Entity entity = this;

	}

	@Override
	public void fall(float l, float d) {
		super.fall(l, d);
		int i = (int) this.posX;
		int j = (int) this.posY;
		int k = (int) this.posZ;
		super.fall(l, d);
		Entity entity = this;

	}

	@Override
	public void onDeath(DamageSource source) {
		super.onDeath(source);
		int i = (int) this.posX;
		int j = (int) this.posY;
		int k = (int) this.posZ;
		Entity entity = this;

	}

	@Override
	public boolean processInteract(EntityPlayer entity, EnumHand hand) {
		super.processInteract(entity, hand);
		int i = (int) this.posX;
		int j = (int) this.posY;
		int k = (int) this.posZ;

		return true;
	}

	@Override
	protected float getSoundVolume() {
		return 1.0F;
	}

	/**
	 * Alexion - Creepinson Created using Tabula 5.1.0
	 */
	public static class ModelAlexion extends ModelBase {
		public ModelRenderer BackR;
		public ModelRenderer FrontLeg;
		public ModelRenderer BackL;
		public ModelRenderer Head;
		public ModelRenderer Body;

		public ModelAlexion() {
			this.textureWidth = 128;
			this.textureHeight = 128;
			this.FrontLeg = new ModelRenderer(this, 0, 51);
			this.FrontLeg.setRotationPoint(-2.0F, 16.0F, -5.6F);
			this.FrontLeg.addBox(0.0F, 0.0F, 0.0F, 3, 8, 3, 0.0F);
			this.BackL = new ModelRenderer(this, 102, 95);
			this.BackL.setRotationPoint(1.9F, 16.0F, 1.2F);
			this.BackL.addBox(0.0F, 0.0F, 0.0F, 3, 8, 3, 0.0F);
			this.BackR = new ModelRenderer(this, 32, 95);
			this.BackR.setRotationPoint(-5.8F, 16.0F, 1.2F);
			this.BackR.addBox(0.0F, 0.0F, 0.0F, 3, 8, 3, 0.0F);
			this.Body = new ModelRenderer(this, 80, 0);
			this.Body.setRotationPoint(-4.3F, 9.0F, -7.0F);
			this.Body.addBox(0.0F, 0.0F, 0.0F, 8, 7, 14, 0.0F);
			this.Head = new ModelRenderer(this, 0, 0);
			this.Head.setRotationPoint(-3.5F, 3.1F, -11.7F);
			this.Head.addBox(0.0F, 0.0F, 0.0F, 6, 6, 6, 0.0F);
		}

		@Override
		public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
			this.FrontLeg.render(f5);
			this.BackL.render(f5);
			this.BackR.render(f5);
			this.Body.render(f5);
			this.Head.render(f5);
		}

		/**
		 * This is a helper function from Tabula to set the rotation of model parts
		 */
		public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		@Override
		public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw,
				float headPitch, float scaleFactor, Entity entityIn) {
			this.Head.rotateAngleX = headPitch / (180F / (float) Math.PI);
			this.FrontLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.2F * limbSwingAmount;
			this.BackL.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.2F * limbSwingAmount;
			this.BackR.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.2F * limbSwingAmount;

			super.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor,
					entityIn);
		}
	}

}