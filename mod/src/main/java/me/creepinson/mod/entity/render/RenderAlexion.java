package me.creepinson.mod.entity.render;

import me.creepinson.mod.entity.EntityAlexion;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderAlexion extends RenderLiving<EntityAlexion> {
	
	public RenderAlexion(RenderManager rendermanagerIn, ModelBase modelbaseIn, float shadowsizeIn) {
		super(rendermanagerIn, modelbaseIn, shadowsizeIn);
	}

	protected ResourceLocation getEntityTexture(EntityAlexion par1Entity) {
		return new ResourceLocation(ModUtils.MODID, "textures/entity/alexion-texturemap.png");
	}

}
