package me.creepinson.mod.entity;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import me.creepinson.mod.core.MainMod;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILeapAtTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SuppressWarnings("unchecked")
public class EntityCreepino extends EntityMob {

    private static final DataParameter<Integer> SPOT_COLOR = EntityDataManager.<Integer>createKey(EntityCreepino.class,
            DataSerializers.VARINT);
    private static final String[] TEXTURES = new String[]{ModUtils.MODID + "textures/entity/creepino/creepino_white.png",
            ModUtils.MODID + "textures/entity/creepino/creepino_red.png"};
    private String texturePrefix;
    private static final String[] TEXTURES_ABBR = new String[]{"cw", "cr"};

    public int getTextureType() {
        return this.dataManager.get(SPOT_COLOR);
    }

    World world = null;

    public EntityCreepino(World var1) {
        super(var1);
        world = var1;
        this.isImmuneToFire = false;
        addRandomArmor();

        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(6, new EntityAIWander(this, 1.0D));
        this.tasks.addTask(8, new EntityAILookIdle(this));
        this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(3, new EntityAIAttackMelee(this, 1.0D, false));
        this.tasks.addTask(4, new EntityAILeapAtTarget(this, 0.42F));
        this.targetTasks.addTask(6, new EntityAINearestAttackableTarget(this, EntityPlayerMP.class, true));
        this.targetTasks.addTask(6, new EntityAINearestAttackableTarget(this, EntityPlayer.class, true));
        this.targetTasks.addTask(3, new EntityAINearestAttackableTarget(this, EntityChicken.class, true));
        this.targetTasks.addTask(5, new EntityAIHurtByTarget(this, false));
        this.tasks.addTask(1, new EntityAILookIdle(this));
        this.tasks.addTask(1, new EntityAIWander(this, 0.8D));
        this.tasks.addTask(6, new EntityAIAttackMelee(this, 1.0D, false));

    }

    // TODO: add entity ai to eat raw chicken items on ground

    public void setSpot(int variant) {
        this.dataManager.set(SPOT_COLOR, Integer.valueOf(variant));
    }

    public int getSpot() {
        return ((Integer) this.dataManager.get(SPOT_COLOR)).intValue();
    }

    protected void entityInit() {
        super.entityInit();
        int n = ThreadLocalRandom.current().nextInt(0, 1 + 1);

        this.dataManager.register(SPOT_COLOR, 0);
        this.setSpot(n);
    }

    public void writeEntityToNBT(NBTTagCompound compound) {
        super.writeEntityToNBT(compound);
        compound.setInteger("Variant", this.getSpot());
    }

    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);
        this.setSpot(compound.getInteger("Variant"));
    }

    private void resetTexturePrefix() {
        this.texturePrefix = null;
    }

    @SideOnly(Side.CLIENT)
    private void setTexturePaths() {
        int i = this.getSpot();

        this.texturePrefix = TEXTURES[i];

    }

    @SideOnly(Side.CLIENT)
    public String getTexture() {
        if (this.texturePrefix == null) {
            this.setTexturePaths();
        }

        return this.texturePrefix;
    }

    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.5D);
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20D);
        if (this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE) != null)
            this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(1.0D);
    }

    protected void addRandomArmor() {

    }

    public void onLivingUpdate() {

        super.onLivingUpdate();
        World world = this.world;
        Random ran = this.rand;
        if (true)
            for (int l = 0; l < 6; ++l) {
                world.spawnParticle(EnumParticleTypes.SLIME, posX, posY, posZ, 0.0D, 0.0D, 0.0D);
            }
    }

    @Override
    protected ResourceLocation getLootTable() {
        return new ResourceLocation(ModUtils.MODID, "creepino_loot");

    }

    @Override
    protected net.minecraft.util.SoundEvent getAmbientSound() {
        return MainMod.CREEPINO_SCREECH;
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return MainMod.CREEPINO_HURT;

    }

    @Override
    protected net.minecraft.util.SoundEvent getDeathSound() {
        return MainMod.CREEPINO_DEATH;
    }

    @Override
    public void onStruckByLightning(EntityLightningBolt entityLightningBolt) {
        super.onStruckByLightning(entityLightningBolt);
        int i = (int) this.posX;
        int j = (int) this.posY;
        int k = (int) this.posZ;
        Entity entity = this;

        j += 1;

        entity.motionY += 0.15D;

    }

    @Override
    protected boolean processInteract(EntityPlayer entity, EnumHand hand) {
        super.processInteract(entity, hand);
        int i = (int) this.posX;
        int j = (int) this.posY;
        int k = (int) this.posZ;

        return true;
    }

    public static class ModelCreepino extends ModelBase {
        public ModelRenderer Body;
        public ModelRenderer Leg;
        public ModelRenderer Head;

        public ModelCreepino() {
            this.textureWidth = 128;
            this.textureHeight = 128;
            this.Leg = new ModelRenderer(this, 39, 44);
            this.Leg.setRotationPoint(-2.0F, 9.0F, -2.0F);
            this.Leg.addBox(0.0F, 0.0F, 0.0F, 4, 15, 4, 0.0F);
            this.Body = new ModelRenderer(this, 92, 0);
            this.Body.setRotationPoint(-4.0F, -5.2F, -4.0F);
            this.Body.addBox(0.0F, 0.0F, 0.0F, 8, 14, 8, 0.0F);
            this.Head = new ModelRenderer(this, 0, 8);
            this.Head.setRotationPoint(-3.0F, -11.2F, -3.0F);
            this.Head.addBox(0.0F, 0.0F, 0.0F, 6, 6, 6, 0.0F);
            this.setRotateAngle(Head, 0.0F, -0.03717551306747922F, 0.0F);
        }

        @Override
        public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
            super.render(entity, f, f1, f2, f3, f4, f5);
            this.Leg.render(f5);
            this.Body.render(f5);
            this.Head.render(f5);
        }

        /**
         * This is a helper function from Tabula to set the rotation of model parts
         */
        public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
            modelRenderer.rotateAngleX = x;
            modelRenderer.rotateAngleY = y;
            modelRenderer.rotateAngleZ = z;
        }

        @Override
        public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw,
                                      float headPitch, float scaleFactor, Entity entityIn) {
            this.Head.rotateAngleX = headPitch / (180F / (float) Math.PI);
            this.Leg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.2F * limbSwingAmount;
            super.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor,
                    entityIn);
        }

    }
}