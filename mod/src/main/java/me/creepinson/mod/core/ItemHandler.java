package me.creepinson.mod.core;

import me.creepinson.mod.core.EnumHandler.BaseTypes;
import me.creepinson.mod.core.EnumHandler.Chips;
import me.creepinson.mod.core.EnumHandler.Circuits;
import me.creepinson.mod.core.EnumHandler.Cores;
import me.creepinson.mod.core.EnumHandler.Essences;
import me.creepinson.mod.core.EnumHandler.SyringeTypes;
import me.creepinson.mod.core.EnumHandler.UpgradeTypes;
import me.creepinson.mod.core.EnumHandler.Wires;
import me.creepinson.mod.item.CreepoMatic;
import me.creepinson.mod.item.ItemBase;
import me.creepinson.mod.item.ItemChip;
import me.creepinson.mod.item.ItemCircuit;
import me.creepinson.mod.item.ItemCore;
import me.creepinson.mod.item.ItemEssence;
import me.creepinson.mod.item.ItemKey;
import me.creepinson.mod.item.ItemLargeBone;
import me.creepinson.mod.item.ItemLimeTriangle;
import me.creepinson.mod.item.ItemStickOfLightning;
import me.creepinson.mod.item.ItemSyringe;
import me.creepinson.mod.item.ItemUpgrade;
import me.creepinson.mod.item.ItemWire;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.registries.IForgeRegistry;

public class ItemHandler {

    public static ItemBase StickOfLightning;

    // ESSENCEPLUS SCIENCE
    public static ItemBase boneLarge;
    public static ItemBase Syringe;

    // ESSENCEPLUS TECH
    public static ItemBase EnergyTablet;
    public static ItemBase Battery;
    public static ItemBase CPU;
    public static ItemBase StorageComponent;
    public static ItemBase HardDrive;
    public static ItemBase Monitor;
    public static ItemBase Chip;
    public static ItemBase Wire;
    public static ItemBase Circut;
    public static ItemBase ingotCopper;
    public static Item upgrade;
    public static Item key;

    // ESSENCEPLUS BASE
    public static ItemBase Core;
    public static ItemBase Essence;
    public static ItemBase ButtKicker;
    public static ItemBase limeTriangle;
    public static CreepoMatic creepoMatic;

    public static void init() {
        creepoMatic = new CreepoMatic("creepomatic", TabHandler.ESSENCEPLUS_BASE);
        Essence = new ItemEssence("essence", TabHandler.ESSENCEPLUS_BASE);
        Core = new ItemCore("core", TabHandler.ESSENCEPLUS_BASE);
        ingotCopper = new ItemBase("ingot_copper").setCreativeTab(CreativeTabs.MATERIALS);
        limeTriangle = new ItemLimeTriangle("lime_triangle");
        StickOfLightning = new ItemStickOfLightning("stickoflightning", TabHandler.ESSENCEPLUS_BASE)
                .setMaxStackSize(1);
        boneLarge = new ItemLargeBone("large_bone", TabHandler.ESSENCEPLUS_BASE);
        upgrade = new ItemUpgrade("upgrade", TabHandler.ESSENCEPLUS_BASE);
        key = new ItemKey("key", TabHandler.ESSENCEPLUS_BASE);
        Chip = new ItemChip("chip", TabHandler.ESSENCEPLUS_BASE);
        Wire = new ItemWire("wire", TabHandler.ESSENCEPLUS_BASE);
        Circut = new ItemCircuit("circuit", TabHandler.ESSENCEPLUS_BASE);

        Syringe = new ItemSyringe("syringe", TabHandler.ESSENCEPLUS_BASE).setMaxStackSize(1);
    }

    public static void register(IForgeRegistry<Item> registry) {

        registry.registerAll(creepoMatic, limeTriangle, ingotCopper, Essence, Core, StickOfLightning, boneLarge, Syringe, Chip, Circut, Wire);
    }

    public static void registerModels() {
        ingotCopper.registerItemModel();
        limeTriangle.registerItemModel();
        creepoMatic.registerItemModel();
        StickOfLightning.registerItemModel();
        boneLarge.registerItemModel();
        ;

        for (int i = 0; i < SyringeTypes.values().length; i++) {
            MainMod.proxy.registerItemRenderer(Syringe, i, "syringe_" + EnumHandler.SyringeTypes.values()[i].getName());
        }

        for (int i = 0; i < Chips.values().length; i++) {
            MainMod.proxy.registerItemRenderer(Chip, i, "chip" + EnumHandler.Chips.values()[i].getName());
        }

        for (int i = 0; i < Circuits.values().length; i++) {
            MainMod.proxy.registerItemRenderer(Circut, i, "circuit" + EnumHandler.Circuits.values()[i].getName());
        }

        for (int i = 0; i < Wires.values().length; i++) {
            MainMod.proxy.registerItemRenderer(Wire, i, "wire" + EnumHandler.Wires.values()[i].getName());
        }

        for (int i = 0; i < BaseTypes.values().length; i++) {
            MainMod.proxy.registerItemRenderer(key, i, EnumHandler.BaseTypes.values()[i].getName() + "key");
        }
        for (int i = 0; i < UpgradeTypes.values().length; i++) {
            MainMod.proxy.registerItemRenderer(upgrade, i, EnumHandler.UpgradeTypes.values()[i].getName() + "upgrade");
        }

        for (int i = 0; i < Cores.values().length; i++) {
            MainMod.proxy.registerItemRenderer(Core, i, EnumHandler.Cores.values()[i].getName() + "core");
        }
        for (int i = 0; i < Essences.values().length; i++) {
            MainMod.proxy.registerItemRenderer(Essence, i, EnumHandler.Essences.values()[i].getName() + "essence");
        }
    }

    public static void oreDictionary() {
        ingotCopper.registerToOreDictionary("ingotCopper", 0);
    }

}