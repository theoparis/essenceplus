package me.creepinson.mod.core;

import java.io.File;
import java.util.Random;

import me.creepinson.mod.util.ModUtils;
import org.apache.logging.log4j.Logger;
import me.creepinson.mod.util.proxy.CommonProxy;
import me.creepinson.mod.world.biome.EPBiomeRegistry;
import me.creepinson.mod.world.dimension.EPDimensionRegistry;
import me.creepinson.mod.world.generation.WorldHandlerVillageDistrict;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod(modid = ModUtils.MODID, name = ModUtils.MODNAME, version = ModUtils.VERSION)
@Mod.EventBusSubscriber
public class MainMod {

    private static Logger logger;

    @Mod.Instance(ModUtils.MODID)
    public static MainMod instance;

    @SidedProxy(serverSide = ModUtils.SERVERPROXY, clientSide = ModUtils.CLIENTPROXY)
    public static CommonProxy proxy;

    private static File configDir;

    public static File getConfigDir() {

        return configDir;
    }

    public static SoundEvent CRAEOL_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MODID, "craeol.death"));
    public static SoundEvent CRAEOL_LIVING = new SoundEvent(new ResourceLocation(ModUtils.MODID, "craeol.ambient"));
    public static SoundEvent CRAEOL_HURT = new SoundEvent(new ResourceLocation(ModUtils.MODID, "craeol.hurt"));
    public static SoundEvent ALEXION_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MODID, "alexion.death"));
    public static SoundEvent ALEXION_LIVING = new SoundEvent(new ResourceLocation(ModUtils.MODID, "alexion.ambient"));
    public static SoundEvent ALEXION_HURT = new SoundEvent(new ResourceLocation(ModUtils.MODID, "alexion.hurt"));
    public static SoundEvent NEOTE_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MODID, "neote.death"));
    public static SoundEvent NEOTE_LIVING = new SoundEvent(new ResourceLocation(ModUtils.MODID, "neote.ambient"));
    public static SoundEvent NEOTE_HURT = new SoundEvent(new ResourceLocation(ModUtils.MODID, "neote.hurt"));
    public static SoundEvent CREEPINO_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MODID, "creepinodeath"));
    public static SoundEvent CREEPINO_HURT = new SoundEvent(new ResourceLocation(ModUtils.MODID, "creepinohurt"));
    public static SoundEvent CREEPINO_SCREECH = new SoundEvent(new ResourceLocation(ModUtils.MODID, "creepinoscreech"));
    public static SoundEvent CREEPERINO_LIVING = new SoundEvent(new ResourceLocation(ModUtils.MODID, "creeprliving"));
    public static SoundEvent CREEPERINO_DEATH = new SoundEvent(new ResourceLocation(ModUtils.MODID, "creeprdeath"));

    @EventHandler
    public void severStarting(FMLServerStartingEvent event) {

    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {

        logger = event.getModLog();
        configDir = new File(event.getModConfigurationDirectory() + "/" + ModUtils.MODID);
        configDir.mkdir();

        ConfigHandler.init(new File(configDir.getPath(), ModUtils.MODID + ".cfg"));

        WorldHandlerVillageDistrict.preInit();

        proxy.preInit();
        EntityHandler.registerEntities();

        LootTableList.register(new ResourceLocation(ModUtils.MODID, "creepino_loot"));
        LootTableList.register(new ResourceLocation(ModUtils.MODID, "alexion_loot"));

        proxy.registerRenderers(this);

    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {

        ItemHandler.oreDictionary();

        EPDimensionRegistry.mainRegistry();
        EPBiomeRegistry.mainRegistry();

        proxy.registerModelBakeryVarients();

        EntityHandler.addSpawns();
        WorldHandlerVillageDistrict.registerComponent(WorldHandlerVillageDistrict.Wall.class, 20, 1, 1);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        BlockHandler.register(event.getRegistry());
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        ItemHandler.init();
        ItemHandler.register(event.getRegistry());
        BlockHandler.registerItemBlocks(event.getRegistry());
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public static void registerModels(ModelRegistryEvent event) {
        BlockHandler.registerModels();
        ItemHandler.registerModels();

    }

}
