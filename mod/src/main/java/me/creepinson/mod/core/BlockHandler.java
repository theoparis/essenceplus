package me.creepinson.mod.core;

import me.creepinson.mod.block.BlockAnalyzer;
import me.creepinson.mod.block.BlockBase;
import me.creepinson.mod.block.BlockCreepolaGrass;
import me.creepinson.mod.block.BlockCreepolaPortal;
import me.creepinson.mod.block.BlockCreepySnow;
import me.creepinson.mod.tileentity.TileEntityAnalyzer;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

public class BlockHandler {

	public static BlockBase.Ore oreCopper = new BlockBase.Ore("ore_copper", CreativeTabs.BUILDING_BLOCKS, 3.5F, 1.8F);
	public static BlockAnalyzer analyzer = new BlockAnalyzer(TabHandler.ESSENCEPLUS_BASE, 3.5f, 8f, 2, "pickaxe");
	public static BlockBase.Ore ore_steel = new BlockBase.Ore("ore_steel", TabHandler.ESSENCEPLUS_BASE, 6.5F, 3.0F, 2,
			"pickaxe");
	public static BlockCreepolaPortal creepola_portal = new BlockCreepolaPortal();
	public static BlockCreepolaGrass creepola_grass = new BlockCreepolaGrass();
	public static BlockBase creepola_dirt = new BlockBase(Material.GRASS, "creepola_dirt",
			TabHandler.ESSENCEPLUS_BASE);
	public static BlockCreepySnow creepySnow = new BlockCreepySnow("creepy_snow").setHardness(0.2F);

	public static void register(IForgeRegistry<Block> registry) {
		registry.registerAll(oreCopper, analyzer, creepola_portal, creepola_dirt, creepola_grass, ore_steel, creepySnow);
	}

	public static void registerItemBlocks(IForgeRegistry<Item> registry) {
		registry.registerAll(oreCopper.createItemBlock(), analyzer.createItemBlock(),
				new ItemBlock(creepola_portal).setRegistryName(creepola_portal.getRegistryName()),
				creepola_dirt.createItemBlock(), creepola_grass.createItemBlock(),
				creepySnow.createItemBlock(), ore_steel.createItemBlock());
		GameRegistry.registerTileEntity(TileEntityAnalyzer.class, analyzer.getRegistryName().toString());
	}

	public static void registerModels() {
		oreCopper.registerItemModel();
		analyzer.registerItemModel();
		creepola_dirt.registerItemModel();
		creepola_grass.registerItemModel();
		creepySnow.registerItemModel();
		ore_steel.registerItemModel();
	}

}