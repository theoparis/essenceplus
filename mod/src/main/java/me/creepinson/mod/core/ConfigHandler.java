package me.creepinson.mod.core;

import java.io.File;

import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.config.Configuration;

public class ConfigHandler {

	public static Configuration config;
	public static File configFile;

	// Biomes

	public static int creepopBiomeID;

	// Dimensions
	public static int creepolaDimensionID = DimensionManager.getNextFreeDimId();

	public static void init(File file) {
		configFile = file;
		config = new Configuration(file);
		syncConfig();
	}

	public static void syncConfig() {
		String category;
		category = "Biomes";

		if(!configFile.exists()){
			config.addCustomCategoryComment(category, "Biome Settings");
			config.save();
		}

		creepopBiomeID = config.getInt("creepopBiomeID", category, 43, 1, 256, "The biome ID of the Creepop Biome.");


	}

}
