package me.creepinson.mod.core;

import me.creepinson.mod.creativetab.EssencePlusBaseTab;
import net.minecraft.creativetab.CreativeTabs;

/**
 * Made By Creepinson
 */
public class TabHandler {
    public static final CreativeTabs ESSENCEPLUS_BASE = (new EssencePlusBaseTab());
}
