package me.creepinson.mod.core;

import me.creepinson.mod.entity.EntityAlexion;
import me.creepinson.mod.entity.EntityColoredShape;
import me.creepinson.mod.entity.EntityAlexion.ModelAlexion;
import me.creepinson.mod.entity.EntityCreepino;
import me.creepinson.mod.entity.EntityCreepino.ModelCreepino;
import me.creepinson.mod.entity.boss.EntityCreeperinoBoss;
import me.creepinson.mod.entity.boss.EntityCreeperinoBoss.ModelCreeperinoBoss;
import me.creepinson.mod.entity.render.RenderAlexion;
import me.creepinson.mod.entity.render.RenderCreeperinoBoss;
import me.creepinson.mod.entity.render.RenderCreepino;
import me.creepinson.mod.entity.render.RenderThrowableTriangle;
import me.creepinson.mod.util.ModUtils;
import me.creepinson.mod.world.biome.EPBiomeRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.init.Biomes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Random;

public class EntityHandler {

	@SideOnly(Side.CLIENT)
	public static void registerRenderers() {

		RenderingRegistry.registerEntityRenderingHandler(EntityCreepino.class, new IRenderFactory<EntityCreepino>() {
			@Override
			public Render<? super EntityCreepino> createRenderFor(RenderManager manager) {
				return new RenderCreepino(manager, new ModelCreepino(), 0.5f);
			}
		});

		RenderingRegistry.registerEntityRenderingHandler(EntityAlexion.class, new IRenderFactory<EntityAlexion>() {
			@Override
			public Render<? super EntityAlexion> createRenderFor(RenderManager manager) {
				return new RenderAlexion(manager, new ModelAlexion(), 0.5F);
			}
		});

		RenderingRegistry.registerEntityRenderingHandler(EntityColoredShape.class,
				new IRenderFactory<EntityColoredShape>() {
					@Override
					public Render<? super EntityColoredShape> createRenderFor(RenderManager manager) {
						return new RenderThrowableTriangle(manager, ItemHandler.limeTriangle,
								Minecraft.getMinecraft().getRenderItem());
					}
				});

		// Bosses
		RenderingRegistry.registerEntityRenderingHandler(EntityCreeperinoBoss.class,
				new IRenderFactory<EntityCreeperinoBoss>() {
					@Override
					public Render<? super EntityCreeperinoBoss> createRenderFor(RenderManager manager) {
						return new RenderCreeperinoBoss(manager, new ModelCreeperinoBoss(), 0.5f);
					}
				});

	}

	public static void registerEntities() {
		registerEntity(EntityCreepino.class, "creepino", (255 << 8) + 51,
				(204 << 16));
		registerEntity(EntityAlexion.class, "alexion", (204 << 16) + 204,
				(255 << 16) + (51 << 8) + 102);
		registerEntity(EntityColoredShape.class, "colored_shape");

		// Bosses

		registerEntity(EntityCreeperinoBoss.class, "boss_creeperino");

	}

	public static void addSpawns() {
		EntityRegistry.addSpawn(EntityCreepino.class, 38, 2, 6, EnumCreatureType.MONSTER,
				Biome.REGISTRY.getObject(Biomes.JUNGLE.getRegistryName()));
		EntityRegistry.addSpawn(EntityCreepino.class, 38, 2, 6, EnumCreatureType.MONSTER,
				Biome.REGISTRY.getObject(Biomes.JUNGLE_HILLS.getRegistryName()));
		EntityRegistry.addSpawn(EntityCreepino.class, 38, 2, 6, EnumCreatureType.MONSTER,
				Biome.REGISTRY.getObject(Biomes.FOREST.getRegistryName()));
		EntityRegistry.addSpawn(EntityCreepino.class, 38, 2, 6, EnumCreatureType.MONSTER,
				Biome.REGISTRY.getObject(Biomes.SWAMPLAND.getRegistryName()));
		EntityRegistry.addSpawn(EntityCreepino.class, 38, 2, 6, EnumCreatureType.MONSTER,
				Biome.REGISTRY.getObject(Biomes.FOREST_HILLS.getRegistryName()));
		EntityRegistry.addSpawn(EntityCreepino.class, 40, 4, 6, EnumCreatureType.MONSTER, EPBiomeRegistry.creepop);
		EntityRegistry.addSpawn(EntityAlexion.class, 32, 3, 18, EnumCreatureType.CREATURE,
				Biome.REGISTRY.getObject(Biomes.EXTREME_HILLS.getRegistryName()));
	}

	public static void registerEntity(Class<? extends Entity> entityClass, String name, int color1, int color2) {
		int entityID = MathHelper.getRandomUUID().hashCode();
		long seed = name.hashCode();
		Random rand = new Random(seed);
		int primaryColor = rand.nextInt() * 16777215;
		int secondaryColor = rand.nextInt() * 16777215;

		EntityRegistry.registerModEntity(new ResourceLocation(ModUtils.MODID, name), entityClass, name, entityID, MainMod.instance,
				50, 1, true, color1, color2);
	}

	public static void registerEntity(Class<? extends Entity> entityClass, String name) {
		int entityID = MathHelper.getRandomUUID().hashCode();
		long seed = name.hashCode();
		Random rand = new Random(seed);
		int primaryColor = rand.nextInt() * 16777215;
		int secondaryColor = rand.nextInt() * 16777215;

		EntityRegistry.registerModEntity(new ResourceLocation(ModUtils.MODID, name), entityClass, name, entityID, MainMod.instance,
				50, 1, true);

	}

}
