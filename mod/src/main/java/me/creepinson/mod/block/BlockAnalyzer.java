package me.creepinson.mod.block;

import me.creepinson.mod.core.ItemHandler;
import me.creepinson.mod.tileentity.TileEntityAnalyzer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockAnalyzer extends BlockBase implements ITileEntityProvider

{

	public BlockAnalyzer(CreativeTabs tab, float hardness, float resistance, int harvest, String tool) {
		super(Material.IRON, "analyzer", tab, hardness, resistance, harvest, tool);

	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityAnalyzer();
	}

	@Override
	public void onBlockHarvested(World worldIn, BlockPos pos, IBlockState state, EntityPlayer player) {
		TileEntityAnalyzer tile = (TileEntityAnalyzer) worldIn.getTileEntity(pos);
		if (tile.getSizeInventory() > 0) {
			InventoryHelper.dropInventoryItems(worldIn, pos, tile);
		}
		worldIn.removeTileEntity(pos);
		super.onBlockHarvested(worldIn, pos, state, player);
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand,
			EnumFacing facing, float hitX, float hitY, float hitZ) {
		if (!world.isRemote && world.getTileEntity(pos) != null && !world.getTileEntity(pos).isInvalid()) {
			TileEntityAnalyzer tile = (TileEntityAnalyzer) world.getTileEntity(pos);
			if (!player.isSneaking()) {
				if (tile.getStackInSlot(0) == ItemStack.EMPTY) {
					if (player.getHeldItemMainhand().getItem() == ItemHandler.Syringe
							&& player.getHeldItemMainhand().getMetadata() != 0) {
						ItemStack i = player.getHeldItemMainhand().copy();
						player.getHeldItemMainhand().setCount(0);
						tile.setInventorySlotContents(0, i);
					}
				}
			} else {
				if (tile.getStackInSlot(0) != ItemStack.EMPTY) {
					tile.analyze();
				}
			}
		}
		return true;
	}

	@Override
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}

}
