package me.creepinson.mod.block;

import me.creepinson.mod.core.MainMod;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

public class BlockBase extends Block {

	public BlockBase(Material mat, String name, CreativeTabs tab, float hardness, float resistance, int harvest,
			String tool) {
		this(mat, name, tab);
		setHardness(hardness);
		setResistance(resistance);
		setHarvestLevel(tool, harvest);
	}

	public BlockBase(Material mat, String name, CreativeTabs tab, float hardness, float resistance) {
		this(mat, name, tab);
		setHardness(hardness);
		setResistance(resistance);
	}

	public BlockBase(Material mat, String name, CreativeTabs tab) {
		this(mat, name);
		setCreativeTab(tab);
	}

	public BlockBase(Material mat, String name) {
		super(mat);
		setRegistryName(ModUtils.MODID, name);
		setTranslationKey(name);
	}

	public Item createItemBlock() {
		Item item = new ItemBlock(this);
		item.setRegistryName(this.getRegistryName());
		return item;
	}
	
	public BlockBase setCreativeTab(CreativeTabs tab) {
		super.setCreativeTab(tab);
		return this;
	}

	public void registerItemModel() {
		MainMod.proxy.registerItemRenderer(Item.getItemFromBlock(this), 0, this.getRegistryName().getPath());
	}
	
	public static class Ore extends BlockBase {
		public Ore(String name, CreativeTabs tab, float hardness, float resistance, int harvest, String tool) {
			super(Material.ROCK, name, tab, hardness, resistance, harvest, tool);
		}

		public Ore(String name, CreativeTabs tab, float hardness, float resistance) {
			super(Material.ROCK, name, tab, hardness, resistance, 1, "pickaxe");
		}

	}
}
