
package me.creepinson.mod.block.pedestal;

import me.creepinson.mod.core.MainMod;
import me.creepinson.mod.block.BlockBase;
import me.creepinson.mod.core.TabHandler;
import net.minecraft.block.material.Material;

public class Pedastal_Magic extends BlockBase {

	public Pedastal_Magic(Material material, String name, String tool, int harvest, float hardness, float resistance) {
		super(material, name, TabHandler.ESSENCEPLUS_BASE, hardness, resistance, harvest, tool);

	}

	/*
	 * @Override public TileEntity createNewTileEntity(World worldIn, int meta) {
	 * 
	 * return new TileEntityPedastal_Magic(); }
	 * 
	 * @Override public void harvestBlock(World world, EntityPlayer player, BlockPos
	 * pos, IBlockState state, TileEntity te, ItemStack stack) {
	 * 
	 * if (!world.isRemote) {
	 * 
	 * if (te instanceof TileEntityPedastal_Magic) { TileEntityPedastal_Magic teCv =
	 * (TileEntityPedastal_Magic) te; EntityItem itemDropped = new EntityItem(world,
	 * pos.getX(), pos.getY() + 1, pos.getZ(), teCv.getStack());
	 * 
	 * world.spawnEntity(itemDropped);
	 * 
	 * teCv.setStack(ItemStack.EMPTY); world.removeTileEntity(pos);
	 * super.harvestBlock(world, player, pos, state, te, stack); } } }
	 * 
	 * @SideOnly(Side.CLIENT) public void initModel() {
	 * ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0,
	 * new ModelResourceLocation(getRegistryName(), "inventory")); // Bind our TESR
	 * to our tile entity
	 * ClientRegistry.bindTileEntitySpecialRenderer(TileEntityPedastal_Magic. class,
	 * new TESRPedastal_Magic()); }
	 * 
	 * @Override
	 * 
	 * @SideOnly(Side.CLIENT) public boolean shouldSideBeRendered(IBlockState
	 * blockState, IBlockAccess worldIn, BlockPos pos, EnumFacing side) { return
	 * false; }
	 * 
	 * @Override public boolean isBlockNormalCube(IBlockState blockState) { return
	 * false; }
	 * 
	 * @Override public boolean isOpaqueCube(IBlockState blockState) { return false;
	 * }
	 * 
	 * private TileEntityPedastal_Magic getTE(World world, BlockPos pos) { return
	 * (TileEntityPedastal_Magic) world.getTileEntity(pos); }
	 * 
	 * @Override public boolean onBlockActivated(World world, BlockPos pos,
	 * IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing,
	 * float hitX, float hitY, float hitZ) { if ((world.isRemote) ||
	 * (!world.isRemote)) { TileEntityPedastal_Magic te = getTE(world, pos);
	 * 
	 * if (player.getHeldItem(hand).getItem() == ItemHandler.key &&
	 * player.getHeldItem(hand).getMetadata() == 0) {
	 * 
	 * if (!te.isLocked()) { te.setLocked(true); player.sendMessage(new
	 * TextComponentTranslation(TextFormatting.AQUA + "[EssencePlus] Security> " +
	 * TextFormatting.DARK_RED + "This pedestal has been locked!")); } else {
	 * player.sendMessage(new TextComponentTranslation(TextFormatting.AQUA +
	 * "[EssencePlus] Security> " + TextFormatting.DARK_GREEN +
	 * "This pedestal has been unlocked!")); te.setLocked(false); } }
	 * 
	 * else {
	 * 
	 * if (te.getStack().isEmpty()) { if (te.isLocked()) {
	 * 
	 * player.sendMessage(new TextComponentTranslation(TextFormatting.AQUA +
	 * "[EssencePlus] " + TextFormatting.RED + "This pedestal is locked!"));
	 * 
	 * } else { if (!player.getHeldItem(hand).isEmpty()) { // There is no item in
	 * the pedestal and the player // is holding an item. We move that item // to
	 * the pedestal te.setStack(player.getHeldItem(hand));
	 * player.inventory.setInventorySlotContents(player.inventory.currentItem,
	 * ItemStack.EMPTY); // Make sure the client knows about the changes in // the
	 * player inventory
	 * 
	 * }
	 * 
	 * else if (!te.getStack().isEmpty()) { if (te.isLocked()) {
	 * 
	 * player.sendMessage(new TextComponentTranslation(TextFormatting.AQUA +
	 * "[EssencePlus] " + TextFormatting.RED + "This pedestal is locked!"));
	 * 
	 * } else { // There is a stack in the pedestal. In this // case we remove it
	 * and try to put it in the // players inventory if there is room ItemStack
	 * stack = te.getStack();
	 * 
	 * if (!player.inventory.addItemStackToInventory(stack)) { // Not possible.
	 * Throw item in the world EntityItem entityItem = new EntityItem(world,
	 * pos.getX(), pos.getY() + 1, pos.getZ(), stack);
	 * world.spawnEntity(entityItem); } te.setStack(ItemStack.EMPTY); }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } } } // Return true also on the client to make sure that MC knows we handled
	 * // this and will not try to place // a block on the client return true; }
	 */
}
