package me.creepinson.mod.util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.block.BlockAir;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityFallingBlock;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * @author creepinson
 */
@Mod.EventBusSubscriber
public class CreepinoUtils {

    private static Map<Integer, Boolean> worldsLoaded = new HashMap<>();

    public static Map<Integer, Boolean> getWorldsLoaded() {
        return worldsLoaded;
    }

    @SubscribeEvent
    public static void onWorldLoaded(WorldEvent.Load event) {
        int worldName = event.getWorld().provider.getDimension();
        System.out.println("World has been loaded: " + worldName);
        worldsLoaded.put(worldName, true);
    }

    @SubscribeEvent
    public static void onWorldUnloaded(WorldEvent.Unload event) {
        int worldName = event.getWorld().provider.getDimension();
        System.out.println("World has been unloaded: " + worldName);
        worldsLoaded.put(worldName, false);
    }

    public static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static void scanAndSteal(World world, IInventory tile, BlockPos pos, int radius) {

        if (!world.isRemote) {
            List<BlockPos> scan = scanNearby(pos, radius);
            for (BlockPos scanPos : scan) {
                if (world.getTileEntity(scanPos) instanceof TileEntityChest) {
                    TileEntityChest tec = (TileEntityChest) world.getTileEntity(scanPos);
                    for (int i = 0; i < tec.getSizeInventory(); i++) {
                        if (!tec.getStackInSlot(i).isEmpty()) {
                            addStackToInventory(tile, tec.getStackInSlot(i));
                            tec.removeStackFromSlot(i);
                        }
                    }
                }
            }
        }
    }

    public static ItemStack addStackToInventory(IInventory inv, ItemStack stack) {
        ItemStack itemstack = stack.copy();

        for (int i = 0; i < inv.getSizeInventory() - 1; ++i) {
            ItemStack itemstack1 = inv.getStackInSlot(i);

            if (itemstack1.isEmpty() && !(i > inv.getSizeInventory())) {
                inv.setInventorySlotContents(i, itemstack);
                inv.markDirty();
                return ItemStack.EMPTY;
            }

            if (ItemStack.areItemsEqual(itemstack1, itemstack)) {
                int j = Math.min(inv.getInventoryStackLimit(), itemstack1.getMaxStackSize());
                int k = Math.min(itemstack.getCount(), j - itemstack1.getCount());

                if (k > 0) {
                    itemstack1.grow(k);
                    itemstack.shrink(k);

                    if (itemstack.isEmpty()) {
                        inv.markDirty();
                        return ItemStack.EMPTY;
                    }
                }
            }
        }

        if (itemstack.getCount() != stack.getCount()) {
            inv.markDirty();
        }

        return itemstack;
    }

    public static List<BlockPos> scanNearby(BlockPos pos, int radius) {
        List<BlockPos> scanResult = new ArrayList<BlockPos>();
        for (int x = pos.getX() - radius; x <= pos.getX() + radius; x++) {
            for (int y = pos.getY() - radius; y <= pos.getY() + radius; y++) {
                for (int z = pos.getZ() - radius; z <= pos.getZ() + radius; z++) {
                    scanResult.add(new BlockPos(x, y, z));
                }
            }
        }
        return scanResult;
    }

    public static List<IBlockState> scanNearby(World world, BlockPos pos, int radius) {
        List<IBlockState> scanResult = new ArrayList<IBlockState>();
        List<BlockPos> scans = scanNearby(pos, radius);
        for (BlockPos scanPos : scans) {
            scanResult.add(world.getBlockState(scanPos));
        }
        return scanResult;
    }

    public static void createBlockExplosion(World world, List<BlockPos> blocksToDestroy) {
        for (BlockPos block : blocksToDestroy) {

            float x = (float) -0.5 + (float) (Math.random() * ((0.5 - -0.5) + 1));
            float y = (float) -1 + (float) (Math.random() * ((1 - -1) + 1));
            float z = (float) -0.5 + (float) (Math.random() * ((0.5 - -0.5) + 1));
            EntityFallingBlock fallingBlock = new EntityFallingBlock(world, block.getX(), block.getY(), block.getZ(),
                    world.getBlockState(block));
            fallingBlock.setDropItemsWhenDead(false);
            fallingBlock.fallTime = 4;
            world.spawnEntity(fallingBlock);
            fallingBlock.setVelocity(x, y, z);
            world.setBlockState(block, Blocks.AIR.getDefaultState());
        }
    }

    public static BlockPos getGround(World world, BlockPos pos) {
        BlockPos blockpos;

        for (blockpos = new BlockPos(pos.getX(), world.getSeaLevel(),
                pos.getZ()); !(world.getBlockState(blockpos.up()).getBlock() instanceof BlockAir); blockpos = blockpos
                .up()) {
            ;
        }
        return blockpos;
    }

}
