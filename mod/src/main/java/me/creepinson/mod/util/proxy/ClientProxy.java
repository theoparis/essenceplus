package me.creepinson.mod.util.proxy;

import me.creepinson.mod.core.MainMod;
import me.creepinson.mod.core.EntityHandler;
import me.creepinson.mod.core.ItemHandler;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ClientProxy extends CommonProxy {

    @Override
    public void registerItemRenderer(Item item, int meta, String id) {
        ModelLoader.setCustomModelResourceLocation(item, meta,
                new ModelResourceLocation(new ResourceLocation(ModUtils.MODID, id), "inventory"));
    }

    @Override
    public void registerRenderers(MainMod mod) {
        EntityHandler.registerRenderers();

    }

    @Override
    public void registerModelBakeryVarients() {
        ModelBakery.registerItemVariants(ItemHandler.Essence, new ResourceLocation(ModUtils.MODID, "fireessence"),
                new ResourceLocation(ModUtils.MODID, "bloodessence"),
                new ResourceLocation(ModUtils.MODID, "lifeessence"), new ResourceLocation(ModUtils.MODID, "magicessence"), new ResourceLocation(ModUtils.MODID, "grassessence"));

    }


    @SubscribeEvent

    public static void registerSounds(RegistryEvent.Register<SoundEvent> event) {


        event.getRegistry().registerAll(MainMod.CREEPINO_DEATH.setRegistryName(MainMod.CREEPINO_DEATH.getSoundName()),
                MainMod.CREEPINO_HURT.setRegistryName(MainMod.CREEPINO_HURT.getSoundName()), MainMod.CREEPINO_SCREECH.setRegistryName(MainMod.CREEPINO_SCREECH.getSoundName()),
                MainMod.NEOTE_DEATH.setRegistryName(MainMod.NEOTE_DEATH.getSoundName()), MainMod.NEOTE_LIVING.setRegistryName(MainMod.NEOTE_LIVING.getSoundName()),
                MainMod.NEOTE_HURT.setRegistryName(MainMod.NEOTE_HURT.getSoundName()), MainMod.ALEXION_DEATH.setRegistryName(MainMod.ALEXION_DEATH.getSoundName()),
                MainMod.ALEXION_LIVING.setRegistryName(MainMod.ALEXION_LIVING.getSoundName()), MainMod.ALEXION_HURT.setRegistryName(MainMod.ALEXION_HURT.getSoundName()),
                MainMod.CRAEOL_DEATH.setRegistryName(MainMod.CRAEOL_DEATH.getSoundName()), MainMod.CRAEOL_LIVING.setRegistryName(MainMod.CRAEOL_LIVING.getSoundName()),
                MainMod.CRAEOL_HURT.setRegistryName(MainMod.CRAEOL_HURT.getSoundName()), MainMod.CREEPERINO_LIVING.setRegistryName(MainMod.CREEPERINO_LIVING.getSoundName()),
                MainMod.CREEPERINO_DEATH.setRegistryName(MainMod.CREEPERINO_DEATH.getSoundName()));
    }

}
