package me.creepinson.mod.util;

import java.util.Random;

public class ModUtils {

	public static final String MODID = "@MODID@";
	public static final String MODNAME = "@MODNAME@";
	public static final String VERSION = "@MODVERSION@";
	public static final Random random = new Random();

	public static final String CLIENTPROXY = "me.creepinson.mod.util.proxy.ClientProxy";
	public static final String SERVERPROXY = "me.creepinson.mod.util.proxy.CommonProxy";

}
