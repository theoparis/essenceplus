package me.creepinson.mod.world.biome;

import java.util.Random;

import me.creepinson.mod.block.BlockCreepySnow;
import me.creepinson.mod.core.BlockHandler;
import me.creepinson.mod.entity.EntityCreepino;
import me.creepinson.mod.util.world.WorldGenBlocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

public class BiomeCreepop extends Biome {

	private int interestingTreesChance;

	public BiomeCreepop(BiomeProperties properties) {
		super(properties);
		this.interestingTreesChance = 4;
		this.decorator.deadBushPerChunk = 0;
		this.decorator.reedsPerChunk = 50;
		this.decorator.cactiPerChunk = 0;
		this.topBlock = BlockHandler.creepola_grass.getDefaultState();
		this.fillerBlock = BlockHandler.creepola_dirt.getDefaultState();
		this.decorator.treesPerChunk = 3;
		this.decorator.grassPerChunk = 20;
		this.spawnableCaveCreatureList.clear();
		this.spawnableCreatureList.clear();
		this.spawnableMonsterList.clear();
		this.spawnableWaterCreatureList.clear();
		this.spawnableMonsterList.add(new SpawnListEntry(EntityCreepino.class, 55, 3, 5));
	}

	@Override
	public void decorate(World worldIn, Random rand, BlockPos pos) {
		super.decorate(worldIn, rand, pos);
		if (this.interestingTreesChance != 0) {
			if (rand.nextInt(this.interestingTreesChance) == 2) {
				int i = rand.nextInt(8);
				int j = rand.nextInt(8);
				BlockPos blockpos = pos.add(i, 0, j).up();
				(new WorldGenBlocks(3, 1, 3,
						BlockHandler.creepySnow.getDefaultState().withProperty(BlockCreepySnow.LAYERS, 8)))
								.generate(worldIn, rand, blockpos);
			}
		}

	}

}