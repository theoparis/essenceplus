package me.creepinson.mod.creativetab;

import me.creepinson.mod.core.ItemHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class EssencePlusBaseTab extends CreativeTabs {

	public EssencePlusBaseTab() {
		super("essenceplus_base");
	}

	@Override
	public ItemStack createIcon() {
		return new ItemStack(ItemHandler.Essence);
	}


}