package me.creepinson.mod.item;

import me.creepinson.mod.world.dimension.CreepolaTeleporter;
import me.creepinson.mod.world.dimension.EPDimensionRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class CreepoMatic extends ItemBase {

	public CreepoMatic(String name, CreativeTabs tab) {
		super(name, tab);

	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer user, EnumHand hand) {
		if (!world.isRemote) {
			world.playSound(user, new BlockPos(user.posX, user.posY, user.posZ), SoundEvents.BLOCK_CHEST_OPEN,
					SoundCategory.AMBIENT, 1.0f, itemRand.nextFloat() * 0.4f + 0.8f);
			if (user.dimension == EPDimensionRegistry.CREEPOLA_NORMAL.getId()) {
				user.getServer().getPlayerList().transferPlayerToDimension((EntityPlayerMP) user, 0,
						new CreepolaTeleporter(user.getServer().getWorld(0)));
			} else {
				user.getServer().getPlayerList().transferPlayerToDimension((EntityPlayerMP) user,
						EPDimensionRegistry.creepolaDimensionID, new CreepolaTeleporter(
								user.getServer().getWorld(EPDimensionRegistry.creepolaDimensionID)));
			}
			return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, new ItemStack(this));

		}

		return new ActionResult<ItemStack>(EnumActionResult.PASS, new ItemStack(this));

	}

}
