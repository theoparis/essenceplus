package me.creepinson.mod.item;

import me.creepinson.mod.core.MainMod;
import me.creepinson.mod.core.TabHandler;
import me.creepinson.mod.entity.EntityColoredShape;
import me.creepinson.mod.util.ShapeTypes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class ItemLimeTriangle extends ItemBase {
	public ItemLimeTriangle(String name) {
		super(name, TabHandler.ESSENCEPLUS_BASE);
		this.maxStackSize = 64;
	}

	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand handIn,
			EnumFacing facing, float hitX, float hitY, float hitZ) {
		ItemStack itemstack = player.getHeldItem(handIn);

		if (!player.capabilities.isCreativeMode) {
			itemstack.shrink(1);
		}

		if (!world.isRemote) {
			RayTraceResult ray = player.rayTrace(20, 10);
			EntityColoredShape triangle = new EntityColoredShape(world, player.posX, player.posY + 1, player.posZ,
					ShapeTypes.LIME_TRIANGLE);
			triangle.shoot(player, player.rotationPitch, player.rotationYaw, 0, 1, 0);
			world.spawnEntity(triangle);
			triangle.playSound(SoundEvents.ENTITY_ENDEREYE_LAUNCH, 1.0F, 1.0F);
		}

		return EnumActionResult.SUCCESS;
	}

	/**
	 * Called when the equipped item is right clicked.
	 */
	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand handIn) {
		ItemStack itemstack = player.getHeldItem(handIn);

		if (!player.capabilities.isCreativeMode) {
			itemstack.shrink(1);
		}

		if (!world.isRemote) {
			RayTraceResult ray = player.rayTrace(20, 10);
			EntityColoredShape triangle = new EntityColoredShape(world, player.posX, player.posY + 1, player.posZ,
					ShapeTypes.LIME_TRIANGLE);
			triangle.shoot(player, player.rotationPitch, player.rotationYaw, 0, 1, 0);
			world.spawnEntity(triangle);
			triangle.playSound(SoundEvents.ENTITY_ENDEREYE_LAUNCH, 1.0F, 1.0F);
		}

		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
	}
}