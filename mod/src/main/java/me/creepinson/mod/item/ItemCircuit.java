package me.creepinson.mod.item;

import me.creepinson.mod.core.EnumHandler;
import me.creepinson.mod.core.EnumHandler.Circuits;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class ItemCircuit extends ItemBase {

	public ItemCircuit(String name, CreativeTabs tab) {
		super(name, tab);
		this.setHasSubtypes(true);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		for (int i = 0; i < Circuits.values().length; i++) {
			ItemStack stack = new ItemStack(this, 1, i);
			if (getCreativeTab() != tab)
				return;
			
			items.add(stack);
		}
	}

	@Override
	public String getTranslationKey(ItemStack stack) {
		for (int i = 0; i < Circuits.values().length; i++) {
			if (stack.getItemDamage() == i) {
				return "circuit" + EnumHandler.Circuits.values()[i].getName();
			}

			else {
				continue;
			}

		}
		return "circuit" + EnumHandler.Circuits.basic.getName();

	}

}