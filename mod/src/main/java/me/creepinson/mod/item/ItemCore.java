package me.creepinson.mod.item;

import me.creepinson.mod.core.EnumHandler;
import me.creepinson.mod.core.EnumHandler.Cores;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class ItemCore extends ItemBase {

	public ItemCore(String name, CreativeTabs tab) {
		super(name, tab);
		this.setHasSubtypes(true);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		for (int i = 0; i < Cores.values().length; i++) {
			ItemStack stack = new ItemStack(this, 1, i);
			if (getCreativeTab() != tab)
				return;
			
			items.add(stack);
		}
	}

	@Override
	public String getTranslationKey(ItemStack stack) {
		for (int i = 0; i < Cores.values().length; i++) {
			if (stack.getItemDamage() == i) {
				return EnumHandler.Cores.values()[i].getName() + "core";
			}

			else {
				continue;
			}

		}
		return EnumHandler.Cores.fire.getName() + "core";

	}

}