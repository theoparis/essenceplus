package me.creepinson.mod.item;

import java.util.List;

import me.creepinson.mod.core.EnumHandler;
import me.creepinson.mod.core.EnumHandler.SyringeTypes;
import me.creepinson.mod.core.ItemHandler;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class ItemSyringe extends ItemBase {

	public ItemSyringe(String name, CreativeTabs tab) {
		super(name, tab);
		this.setHasSubtypes(true);

	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		for (int i = 0; i < SyringeTypes.values().length; i++) {
			ItemStack stack = new ItemStack(this, 1, i);
			if (getCreativeTab() != tab)
				return;

			items.add(stack);
		}
	}

	@Override
	public String getTranslationKey(ItemStack stack) {
		for (int i = 0; i < SyringeTypes.values().length; i++) {
			if (stack.getItemDamage() == i) {
				return this.getTranslationKey() + "." + EnumHandler.SyringeTypes.values()[i].getName();
			}

			else {
				continue;
			}

		}

		return this.getTranslationKey() + "." + EnumHandler.SyringeTypes.Empty.getName();
	}

	@Override
	public String getTranslationKey() {
		return name;
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
		if (stack.getTagCompound() != null) {
			if (stack.getItemDamage() != SyringeTypes.Empty.getID()) {
				NBTTagCompound tag = stack.getTagCompound();
				if (tag.hasKey("analyzed")) {
					if (tag.getBoolean("analyzed")) { // TODO: Remove ! from statement after adding analyzer!
						tooltip.add(TextFormatting.WHITE + "Analyzed Data: ");
						tooltip.add(TextFormatting.GOLD + "Blood Source's UUID: " + tag.getUniqueId("source-id"));
						tooltip.add(TextFormatting.GOLD + "Blood Source's Name/Type: " + tag.getString("source-name"));
					}
				}
			}
		}
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

	@Override
	public boolean itemInteractionForEntity(ItemStack item, EntityPlayer player, EntityLivingBase target,
			EnumHand hand) {
		if (item.getItemDamage() == SyringeTypes.Empty.getID()) {
			if (target instanceof EntityZombie) {
				player.inventory.deleteStack(player.getHeldItem(hand));
				ItemStack syringe = new ItemStack(ItemHandler.Syringe, 1, 1);
				NBTTagCompound tag = new NBTTagCompound();
				tag.setUniqueId("source-id", target.getPersistentID());
				tag.setString("source-name", target.getName());
				tag.setBoolean("analyzed", false);
				syringe.setTagCompound(tag);
				player.inventory.addItemStackToInventory(syringe);

				return true;
			} else if (target instanceof EntityPlayer && target.getName() != player.getName()) {
				player.inventory.deleteStack(player.getHeldItem(hand));
				ItemStack syringe = new ItemStack(ItemHandler.Syringe, 1, 2);
				NBTTagCompound tag = new NBTTagCompound();
				tag.setUniqueId("source-id", target.getPersistentID());
				tag.setString("source-name", target.getName());
				tag.setBoolean("analyzed", false);
				syringe.setTagCompound(tag);
				player.inventory.addItemStackToInventory(syringe);

				return true;
			}
		} else {
			return super.itemInteractionForEntity(item, player, target, hand);

		}
		return super.itemInteractionForEntity(item, player, target, hand);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
		{
			if (player.getHeldItemMainhand().getItemDamage() == SyringeTypes.Empty.getID()) {
				RayTraceResult raytrace = player.rayTrace(5, 1);
				if (raytrace.typeOfHit == Type.MISS) {
					player.inventory.deleteStack(player.getHeldItem(hand));
					ItemStack syringe = new ItemStack(ItemHandler.Syringe, 1, 2);
					NBTTagCompound tag = new NBTTagCompound();
					tag.setUniqueId("source-id", player.getPersistentID());
					tag.setString("source-name", player.getName());
					tag.setBoolean("analyzed", false);
					syringe.setTagCompound(tag);
					player.inventory.addItemStackToInventory(syringe);
				}
			}
			return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, player.getHeldItemMainhand());
		}

	}

}
