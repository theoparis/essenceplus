package me.creepinson.mod.item;

import me.creepinson.mod.core.EnumHandler;
import me.creepinson.mod.core.EnumHandler.Wires;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class ItemWire extends ItemBase {

	public ItemWire(String name, CreativeTabs tab) {
		super(name, tab);
		this.setHasSubtypes(true);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		for (int i = 0; i < Wires.values().length; i++) {
			ItemStack stack = new ItemStack(this, 1, i);
			if (getCreativeTab() != tab)
				return;
			
			items.add(stack);
		}
	}
	@Override
	public String getTranslationKey(ItemStack stack) {
		for (int i = 0; i < Wires.values().length; i++) {
			if (stack.getItemDamage() == i) {
				return "wire" + EnumHandler.Wires.values()[i].getName();
			}

			else {
				continue;
			}

		}
		return "wire" + EnumHandler.Wires.copper.getName();

	}

}