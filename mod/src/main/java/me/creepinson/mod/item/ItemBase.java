package me.creepinson.mod.item;

import me.creepinson.mod.core.MainMod;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class ItemBase extends Item {

	protected String name;

	public ItemBase(String name) {
		this.name = name;
		setRegistryName(ModUtils.MODID, name);
		setTranslationKey(name);
	}
	
	public ItemBase(String name, CreativeTabs tab) {
		this.name = name;
		setRegistryName(ModUtils.MODID, name);
		setTranslationKey(name);
		this.setCreativeTab(tab);
	}


	public void registerItemModel() {
		MainMod.proxy.registerItemRenderer(this, 0, name);
	}
	
	@Override
	public ItemBase setCreativeTab(CreativeTabs tab) {
		super.setCreativeTab(tab);
		return this;
	}
	
	public void registerToOreDictionary(String oreDictName, int meta){
		OreDictionary.registerOre(oreDictName, new ItemStack(this, 1, meta));
	}
	@Override
	public ItemBase setMaxStackSize(int maxStackSize) {
		this.maxStackSize = maxStackSize;
		return this;
	}
}