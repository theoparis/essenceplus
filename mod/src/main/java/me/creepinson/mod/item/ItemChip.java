package me.creepinson.mod.item;

import me.creepinson.mod.core.EnumHandler;
import me.creepinson.mod.core.EnumHandler.Chips;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class ItemChip extends ItemBase {

	public ItemChip(String name, CreativeTabs tab) {
		super(name, tab);
		this.setHasSubtypes(true);
	}
	
	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		for (int i = 0; i < Chips.values().length; i++) {
			ItemStack stack = new ItemStack(this, 1, i);
			if (getCreativeTab() != tab)
				return;
			
			items.add(stack);
		}
	}

	@Override
	public String getTranslationKey(ItemStack stack) {
		for (int i = 0; i < Chips.values().length; i++) {
			if (stack.getItemDamage() == i) {
				return "chip" + EnumHandler.Chips.values()[i].getName();
			}

			else {
				continue;
			}

		}
		return "chip" + EnumHandler.Chips.base.getName();

	}

}